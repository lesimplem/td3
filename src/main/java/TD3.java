/**
 *
 * @param a
 * @param b
 * @return le max entre a et b
 */
int max2(int a, int b ) {
    if(a>b){
        return a;
    }
    else {
        return b;
    }
};

/**
 *
 * @param a
 * @param b
 * @param c
 * @return le max entre a, b et c
 */
int max3(int a, int b , int c) {

    throw new RuntimeException("Méthode non implémentée ! Effacez cette ligne et écrivez le code nécessaire");
}

/**
 *
 * @param n
 * @return le nombre de chiffres que comporte l'écriture, en base 10, de n
 */
/**
 *
 * @param n
 * @return le nombre de chiffres que comporte l'écriture du nombre n^2
 */



/**
 * afficher nb caractères car (à partir de la position courante du curseur sur le terminal).
 * @param nb
 * @param car
 */
void  repetCarac(int nb, char car){
    for (int i =0;i<nb;i++){
        Ut.afficher(car);
    }
};

/**
 * affiche à l'écran une pyramide de hauteur h constituée de lignes répétant le caractère c.
 * @param h
 * @param c
 */
void pyramideSimple (int h  , char  c ){
    //l'utilisateur saisie un nombre entier
    for (int i =1;i<=h;i++){
        repetCarac(h-i,' ');
        repetCarac(2*i-1,c);
        Ut.sautLigne();

    }


}


/**
 * affiche à l'écran sur une même ligne les chiffres représentant les unités des nombres allant de nb1 à nb2 en ordre croissant
 * si nb1 < nb2, et ne fait rien sinon.
 * @param nb1
 * @param nb2
 */
void afficheNombresCroissants (int nb1,int nb2 ){
    while(nb1<=nb2) {
        Ut.afficher(nb1 % 10);
    nb1++;
    }
}

/**
 * affiche à l'écran sur une même ligne les chiffres représentant les unités des nombres allant de nb1 à nb2 en ordre décroissant
 * si nb1 < nb2, et ne fait rien sinon.
 * @param nb1
 * @param nb2
 */
void afficheNombresDecroissants (int nb1,int nb2 ){
        while(nb1<=nb2) {
            Ut.afficher(nb2 % 10);
            nb2--;
        }
    }

/**
 * permet de représenter à l'écran la pyramide
 * @param h
 */


void pyramide(int h ) {
    int ligne=0;
    for (int i =1;i<h + 1;i++){
        repetCarac(h-i,' ');
        afficheNombresCroissants(i,i+ligne);
        afficheNombresDecroissants(i,i+ligne-1);
        Ut.sautLigne();
        ligne++;

    }
}




int nbChiffres(int n) {
    //PR: n>0
    int compteur = 1;
    while (n >= 10) {
        n = n / 10;
        compteur += 1;
    }
    return compteur;
};

int nbChiffresDuCarre(int n){
    int n2=n*n;
    return nbChiffres(n2);
}

int racineParfaite(int c ){
    double n = Math.sqrt(c);
    if (n%1==0){
        return (int) n;
    }
    else {
        return -1;
    }
}

/*

 * @param c
 * @return la racine carrée entière n d'un nombre entier c donné,
 * si c est un carré parfait, c'est-à-dire si c = n * n.
 * Sinon la fonction retourne -1.




 * @param nb
 * @return vrai si un entier donné est un carré parfait, faux sinon.
 */
boolean estCarreParfait(int nb){
    if(Math.sqrt(nb)%1==0){
        return true;
    }
    else {
        return false;
    }
}






boolean nombresAmicaux(int p, int q) {
    int somme = 0;
    int somme1 = 0;
    for (int i = 1; i <= p / 2; i++) {
        if (p % i == 0) {
            somme += i;
        }
    }
    for (int i = 1; i <= q / 2; i++) {
        if (q % i == 0) {
            somme1 += i;
        }
    }
    if (somme == q && somme1 == p) {
        return true;
    } else {
        return false;
    }
}


/**

 affiche à l'écran les couples de*nombres amis parmi les nombres inférieurs ou égaux à max.
 @param max*/

void afficheAmicaux(int max) {
    for (int i = 1; i <= max; i++) {
        for (int j = i + 1; j <= max; j++) {
            if (nombresAmicaux(i, j)) {
                Ut.afficherSL(i + " et " + j);
            }
        }
    }
}
void testAmis(int plafond){
    //PR:
    //Action:
    for (int i = 1; i<=plafond; i++){
        for (int j = i+1; j<=i*1.30 ; j++){
            if (nombresAmicaux(i,j)==true){
                Ut.afficherSL("les nombres "+i+" et "+j+" sont amis");
            }
        }
    }
}

boolean testTriangle(int a, int b){
    int c= (a*a)+(b*b);
    if (Math.sqrt(c)%1==0){
        return true;
    }
    else {
        return false;
    }
}
int testTriangle(int c1, int c2){
    int c3 = (c1*c1)+(c2*c2);
    if (Math.sqrt(c3)%1==0){
        return c1;
        return c2;
        return c3;
    }

}
void nbTriangleRectangle (int perimetre){
    int compteur=0;
    for (int i=1;i<perimetre;i++){
        for (int j=1;j<perimetre;j++) {
            if (testTriangle(i, j)) {
                compteur++
            }
        }
    }

}

void main (){
    testAmis(7000);

}
